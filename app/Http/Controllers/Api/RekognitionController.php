<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Aws\Rekognition\RekognitionClient;

class RekognitionController extends \App\Http\Controllers\Controller
{

    public function uploadToS3(Request $request)
    {
        $form = $request->all();

        //s3アップロード開始
        $image = $request->file('image');
        $size = getimagesize($image);
        $width = $size[0];
        $height = $size[1];
        // 表示するサイズ
        $disp_w = 200;
        $disp_h = 150;
        $disp_ratio = $disp_w / $disp_h;
        $img_ratio = $width / $height;
        if ($disp_ratio < $img_ratio) {
            // 横がはみ出る
            $crop_w = $disp_w * $height / $disp_h;
            $crop_h = $height;
        } else {
            // 同じまたは縦がはみ出る
            $crop_w = $width;
            $crop_h = $disp_h * $width / $disp_w;
        }
        $path = Storage::disk('s3')->putFile('', $image, 'public');
        $fullpath = Storage::disk('s3')->url($path);

        $options = [
            'region'      => 'ap-northeast-1',
            'version'     => 'latest',
        ];

        $rekognition = new RekognitionClient($options);
        $result = $rekognition->DetectFaces([
            'Attributes'=> ['ALL'],  //ALL|DEFAULT。ALLだと重い
            'Image' => [
                'Bytes' => file_get_contents($fullpath),
            ],
        ]);

        $datas = [];
        foreach ($result['FaceDetails'] as $key => $face) {
            $data = $face;
            $w = round($width * $face['BoundingBox']['Width']);
            $h = round($height * $face['BoundingBox']['Height']);
            $left = round($width * $face['BoundingBox']['Left']);
            $top = round($height * $face['BoundingBox']['Top']);

            // 顔部分を切り取り
            $faceImg = '<div class="elevation-2" style="margin-right: 15px;border-radius:3px;float:left;margin-bottom:10px;width:'.$w.'px;height:'.$h.'px;background:url('.$fullpath.') no-repeat -'.$left.'px -'.$top.'px"></div>';
            $data["faceImage"] = $faceImg;
            array_push($datas, $data);
        }

        $data = [
            'datas' => $datas,
            'image' => $fullpath,
        ];

        return response()->json($data);
    }

}
