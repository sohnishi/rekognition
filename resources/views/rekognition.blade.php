@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1></h1>
@stop

@section('content')

<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">顔認識を行う</h3>
    </div>

    <form role="form" action="{{ action('App\Http\Controllers\Api\RekognitionController@uploadToS3') }}" method="post" enctype="multipart/form-data">
        <div class="card-body">
          <div class="form-group">
            <label for="exampleInputFile">画像アップロード</label>
            <div class="input-group">
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="image" id="image" accept="image/*">
                {{ csrf_field() }}
                <label class="custom-file-label" for="image">画像を選択してください</label>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
          <button id="upload" type="submit" disabled="disabled" class="btn btn-primary">アップロード</button>
        </div>
    </form>

</div>
    <h5>解析結果</h5>
    <div class="uploadedImageContainer">
        <img id="uploadedImage" src="">
        <div class="faceEnclose"></div>
        <div class="landMark"></div>
    </div>
    <div class="row" id="result">
        <div class="card card-widget col-md-5">
            <div class="card-header">
              <div class="user-block">
                <div class="widget-user-image">
                </div>
                <span class="username"><a href="#"></a></span>
                <span class="description"></span>
              </div>
              <!-- /.user-block -->
              <div class="card-tools">
                {{-- <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button> --}}
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                </button>
              </div>
              <!-- /.card-tools -->
            </div>
            <!-- /.card-body -->
            <div class="card-body" style="display: block;">
                <ul class="nav flex-column">
                    <li class="nav-item" style="display:none;">
                      <a href="#" class="nav-link">
                        <span class="float-right badge bg-primary"></span>
                      </a>
                    </li>
                </ul>
            </div>
            <!-- /.card-body -->
          </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">

    <!-- Styles -->
    <style>
        /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        .uploadedImageContainer {
            position:relative;
        }
        .faceEnclose {
            display: none;
            z-index: 99999;
            position: absolute;
            border: 1px solid red;
        }
        .landMark {
            display: none;
            z-index: 99999;
            position: absolute;
            background-color: white;
            width: 1.5px;
            height: 1.5px;
        }
        #result>.card {
            display:none;
            margin-left:15px;
            margin-right:15px;
            margin-bottom:15px;
        }

        #spin {
            float: right;
            background-color: #007bff;
            margin-left: 5px;
        }

        #spin>i {
            color: white!important;
            line-height: 1.5;
        }
    </style>

@stop

@section('js')
    <script>

        function makeLi(rootKey, data, res, li) {

            for (let key in data) {

                if (typeof data[key] == 'object') {
                    makeLi(rootKey, data[key], res, li);
                } else {
                    // 符号なし小数
                    var reg1 = new RegExp(/^([1-9]\d*|0)(\.\d+)?$/);
                    // 符号あり小数 (+, – 許容)
                    var reg2 = new RegExp(/^[+,-]?([1-9]\d*|0)(\.\d+)?$/);
                    var regres1 = data[key].toString().match(reg1);
                    var regres2 = data[key].toString().match(reg2);
                    if (regres1 || regres2) {
                        res = res + Math.round(data[key] * 10) / 10 + "% ";
                    } else {
                        if (rootKey == "AgeRange") {
                            res = res + data[key] + " - ";
                        } else {
                            res = res + data[key] + " ";
                        }
                    }

                }
            }
            li.html(li.html() + res);
            return res;

        }

        $("#image").change(function() {

            var filename = $("#image").val();
            console.log(filename);
            if (filename) {
                filename = filename.replace("C:\\fakepath\\", "");
                $(this).parent().find("label:first").text(filename);
                $("#upload").prop("disabled", false);
            }

        });
        $("#upload").click(function() {
            $("#result").find(".card:not(:first)").remove();
            //通常のアクションをキャンセルする
            event.preventDefault();
            //Formの参照を取る
            $form = $(this).parents('form:first');
            var fd = new FormData();
            fd.append( "image", $("#image").prop("files")[0] );

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url : $form.attr('action'), //Formのアクションを取得して指定する
                type: $form.attr('method'),//Formのメソッドを取得して指定する
                data: fd,
                processData: false, // jQueryがデータを処理しないよう指定
                contentType: false,  // jQueryがcontentTypeを設定しないよう指定
                timeout: 10000,
                beforeSend : function(xhr, settings){
                    //Buttonを無効にする
                    $('#upload').attr('disabled' , true);
                    //処理中のを通知するアイコンを表示する
                    $('#upload').append('<div class="overlay" id ="spin" name = "spin"><i class="fa fa-redo fa-spin"></i></div>');
                },
                complete: function(xhr, textStatus){
                    //処理中アイコン削除
                    $('#spin').remove();
                    $('#upload').attr('disabled' , false);
                },
                success: function (result, textStatus, xhr) {
                    var uploadedImage = $('#uploadedImage');
                    uploadedImage.attr("src", result.image);
                    var uploadedImageContainer = $('body').find('.uploadedImageContainer:first');
                    var uploadedImageW = 0;
                    var uploadedImageH = 0;
                    $.each(result.datas, function(key, data) {

                        var card = $("#result").find("div:first").clone();
                        card.css('display', 'block');
                        card.find('.widget-user-image:first').html(data["faceImage"]);
                        var footer = card.find('.card-body:first');
                        var ul = footer.find('ul:first');

                        $.each(data, function(k, d) {
                            if (typeof d == 'object' && k != 'faceImage' && k != 'BoundingBox' && k != 'Landmarks') {
                                var li = ul.find('li:first').clone();
                                li.css('display', 'block');
                                li.html(k + ': ');
                                makeLi(k, d, "", li);
                            }



                            uploadedImage.on('load', function(){
                                uploadedImageW = $(this).width();
                                uploadedImageH = $(this).height();
                                if (k == 'BoundingBox') {
                                    var w = Math.round(uploadedImageW * d['Width']);
                                    var h = Math.round(uploadedImageH * d['Height']);
                                    var left = Math.round(uploadedImageW * d['Left']);
                                    var top = Math.round(uploadedImageH * d['Top']);
                                    var faceEnclose = $('body').find('.faceEnclose:first').clone();
                                    faceEnclose.css('display', 'block');
                                    faceEnclose.css('width', w);
                                    faceEnclose.css('height', h);
                                    faceEnclose.css('top', top);
                                    faceEnclose.css('left', left);
                                    uploadedImageContainer.append(faceEnclose);
                                }

                                if (k == 'Landmarks') {
                                    $.each(d, function(key, mark) {
                                        var landMark = $('body').find('.landMark:first').clone();
                                        landMark.css('display', 'block');
                                        var left = Math.round(uploadedImageW * mark['X']);
                                        var top = Math.round(uploadedImageH * mark['Y']);
                                        landMark.css('top', top);
                                        landMark.css('left', left);
                                        uploadedImageContainer.append(landMark);
                                    });
                                }
                            });


                            $(ul).append(li);
                        });
                        $('#result').append(card);
                    });
                },
                error : function(data){
                    $('#btnProfileUpdate').attr('disabled' , false);
                    console.debug(data);
                }
            });
        });

    </script>
@stop
